#!/bin/bash

# Write log to stdout
log () {
  echo " "
  echo "##############################"
  echo "$1"
  echo "##############################"
  echo " "
}

log_simple() {
  echo " "
  echo "------> $1"
  echo " "
}

log "START OF create-terminologit.sh"

# create directory where logs from specific commands will be preserved and will be made accessible via artifacts
# these logs have to be read together with the main pipeline log
log_simple "create logging directory"
rm -rvf log_appendix
mkdir -v log_appendix

# create temporary directory for old versions of resources (content will later be copied to IG's output directory)
log_simple "create directory for old versions"
rm -rvf old
mkdir -v old

# retrieve template for IG
log_simple "clone terminologit-template"
git -c http.Verify=${SECURE_GIT_REQUESTS:-true} clone ${CI_SERVER_PROTOCOL}://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${TERMGIT_TEMPLATE_PROJECT:-elga-gmbh/terminologit-template}.git ./input/terminologit-template || exit 1

# for integration tests (triggered by another pipeline) no IGVer will be executed
if [[ $CI_PIPELINE_SOURCE != "pipeline" ]]
then
  # retrieve old IG's output (necessary for detecting resource changes)
  log_simple "clone ${TERMGIT_HTML_PROJECT} containing old IG's output"
  git -c http.Verify=${SECURE_GIT_REQUESTS:-true} clone ${CI_SERVER_PROTOCOL}://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${TERMGIT_HTML_PROJECT}.git ./termgit-html-project || exit 1

  log "RUN IGVer"

  # make sure ./termgit-html-project/output exists
  mkdir -pv ./termgit-html-project/output/

  # Git SHA of the last commit within this repository (required for creating the downloads)
  # https://www.systutorials.com/how-to-get-the-latest-commits-hash-in-git/
  git log --pretty=%H -1 > tmp_currentCommit.txt
  tr -d '\n' < tmp_currentCommit.txt > currentCommit.txt

  # retrieve terminologiesMetadata.csv from the last successful pipeline run
  git show $(cat lastSuccessfulPipeline.txt):terminologies/terminologiesMetadata.csv > old_terminologiesMetadata.csv

  # copy igVer.py along with required resources to termgit-html-project/output where it will detect if resources have been changed
  log_simple "copy IGVer (+ required module) old IG's output"
  cp -v ./igVer.py ./termgit-html-project/output/
  cp -v ./file_name_and_extension_extractor.py ./termgit-html-project/output/
  log_simple "copy data required for IGVer's execution"
  cp -v ./currentCommit.txt ./termgit-html-project/output/
  cp -v ./old_terminologiesMetadata.csv ./termgit-html-project/output/
  cp -v ./processedTerminologies.csv ./termgit-html-project/output/

  # change into directory of old IG output
  log_simple "change to old IG's output directory"
  echo pwd: $PWD
  cd termgit-html-project/output
  echo pwd: $PWD

  # execute IGVer for detecting changed resources
  log_simple "run IGVer"
  python igVer.py || exit 1
  log_simple "finished IGVer"

  # change into root directory
  log_simple "change into root directory"
  echo pwd: $PWD
  cd ../..
  echo pwd: $PWD

  # remove all content from termgit-html-project
  log_simple "Remove old content from termgit-html-project"
  echo log_appendix/delete_termgit-html-project.log
  rm -rvf ./termgit-html-project > log_appendix/delete_termgit-html-project.log

  log "IGVer FINISHED"
else
  log "Prepare includes for integration test"
  # create list of terminologies within "terminologies/CodeSystem*" and "terminologies/ValueSet*" directories
  find terminologies/CodeSystem* terminologies/ValueSet* -type f > resource_file_path_list.txt

  echo log_appendix/prepare_includes_integration_test.log
  python prepare_includes_integration_test.py > log_appendix/prepare_includes_integration_test.log || exit 1

  echo log_appendix/git_status_integration_test.log
  git status > log_appendix/git_status_integration_test.log
  log "Prepare includes for integration test finished"
fi

log "CREATE IG"

log_simple "copy /README.md and /README_de.md both as arch_and_setup_*.md to /input/pagecontent/"

mkdir -p ./input/pagecontent/

# copy /README.md and /README_de.md both with same filename to /input/pagecontent/
cp "./README.md" "./input/pagecontent/arch_and_setup_en.md"
cp "./README_de.md" "./input/pagecontent/arch_and_setup_de.md"

# if /input/pagecontent/index.md does not exist, copy /README.md to /input/pagecontent/index.md
[ ! -f "./input/pagecontent/index.md" ] && cp "./README_de.md" "./input/pagecontent/index.md" && log_simple "copy /README_de.md to /input/pagecontent/index.md"
[ ! -f "./input/pagecontent/index_en.md" ] && cp "./README.md" "./input/pagecontent/index_en.md" && log_simple "copy /README.md to /input/pagecontent/index_en.md"

# copy .4.fhir.json files that had been changed in the initiating commit to ./input/resources
log_simple "copy .4.fhir.json files that had been changed in the initiating commit to ./input/resources"
mkdir -pv './input/resources'
echo log_appendix/copy_4.fhir.json_to_resources.log
touch log_appendix/copy_4.fhir.json_to_resources.log
# move all files in the list to terminologies (from https://superuser.com/questions/538306/move-a-list-of-filesin-a-text-file-to-a-directory)
for file in $(cat terminology_file_paths.txt); do 
    dir=$(dirname "$file")
    find $dir -name '*.4.fhir.json' -exec cp -prv '{}' './input/resources' ';' >> log_appendix/copy_4.fhir.json_to_resources.log
done


# Remove '.4.fhir' from file names in order to make them detectable by IG publisher
log_simple "remove '.4.fhir' from file names"
echo log_appendix/rename_4.fhir.json_to_json.log
find ./input/resources -type f -name '*.4.fhir.json' | while read FILE ; do
    newfile="$(echo ${FILE} |sed -e 's/.4.fhir//')" ;
    mv -v "${FILE}" "${newfile}" >> log_appendix/rename_4.fhir.json_to_json.log ;
done

# set canoncial for Implementation Guide resource
log_simple "set canoncial to: $TERMGIT_CANONICAL"
sed -i "s|TERMGIT_CANONICAL|$TERMGIT_CANONICAL|" sushi-config.yaml
log_simple "canonical has been set"

# run sushi for creating ImplementationGuid resource
log_simple "run sushi"
sushi || exit 1
log_simple "sushi finished"

# if IG_PUBLISHER_URL is set use the value of the environment variable and download the IG publisher from there
if ! [[ -z "$IG_PUBLISHER_URL" ]];
then
  # download IG publisher
  log_simple "download IG publisher"
  curl -L ${IG_PUBLISHER_URL} -o "$PWD/input-cache/publisher.jar" --create-dirs
  log_simple "IG publisher downloaded"
else
  # copy the IG publisher provided in the docker container
  log_simple "copy IG publisher"
  # make sure ./input-cache/ exists
  mkdir -pv ./input-cache/
  # copy IG publisher
  cp -v /opt/publisher.jar ./input-cache/publisher.jar
  log_simple "IG publisher copied"
fi

# run IG publisher:
log_simple "run IG publisher"
unset DISPLAY
# IG publisher will be executed. If executtion of IG publisher fails, this script will fail and, thus, the pipeline as well.
./_genonce.sh || exit 1
log_simple "IG publisher finished"

# minify all HTML files created by IG publisher
python minify_html_output.py > log_appendix/minify_html_output.log

# copy content from "./old" (contains all old versions of resources) to "./output"
log_simple "copy old versions to output directory"
echo log_appendix/copy_old_to_output.log
cp -v ./old/* ./output/ > log_appendix/copy_old_to_output.log

# create sitemap
log_simple "create and copy information about sitemap to output directory"
python sitemap_generator.py || exit 1
cp -v ./input/sitemap/* ./output/

# create redirects
log_simple "copy redirects for canonicals to output"
cp -vR ./input/redirects/* ./output/

log "IG CREATED"

# for integration tests (triggered by another pipeline) no input/include will not be updated
if [[ $CI_PIPELINE_SOURCE != "pipeline" ]]
then
  log "UPDATE input/includes"

  # stage ./input/sitemap
  git add "./input/sitemap"
  # stage ./input/includes
  git add "./input/includes/*-download.xhtml"
  # stage new previous versions if existent
  git add $(cat new_previous_versions_files.txt)
  gitCommitMessage="${AUTOMATIC_COMMIT_PREFIX} update sitemap, downloads"

  # if the job is executed in default branch stage *-previous-versions.xhtml as well:
  if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]
  then
    git add "./input/includes/*-previous-versions.xhtml"
    gitCommitMessage="${gitCommitMessage}, and previous versions"
  fi
  git status
  git commit -m "${gitCommitMessage}"
  git -c http.Verify=${SECURE_GIT_REQUESTS:-true} push "${CI_SERVER_PROTOCOL}://${GITLAB_USER_LOGIN}:${GITLAB_CI_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${CI_PROJECT_PATH}.git" HEAD:${CI_COMMIT_BRANCH} || exit 1

  log "FINISHED UPDATE input/includes"
else
  log "input/includes not updated: pipeline source = ${CI_PIPELINE_SOURCE}"
fi

# move files that should not be published to a separate directory for them to be available if needed
mkdir removed_from_output
mv -v ./output/*.zip ./output/*.tgz ./output/*.xlsx ./output/*.pack ./output/expansions.* ./output/qa.min.html ./output/qa.xml ./output/qa.txt ./removed_from_output/

log "END OF create-terminologit.sh"
